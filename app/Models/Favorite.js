'use strict'

const Model = use('Model')

class Favorite extends Model {


    blog() {
        return this.belongsTo("App/Models/Blog");
    }

    user() {
        return this.belongsTo('App/Models/User')
    }
}

module.exports = Favorite
